import {RootState} from './store/reducers';

export const getUser = (state: RootState) => state.user;
export const getTodo = (state: RootState) => state.todo;
