import React, {useEffect} from 'react';
import {useActions} from '../hooks/useActions';
import {getTodo} from '../selectors';
import {useSelector} from 'react-redux';

const TodoList = () => {

    const {loading, limit, error, page, todos} = useSelector(getTodo);
    const {fetchTodos, setTodoPage} = useActions();
    const pages = [1, 2, 3, 4, 5, 6];

    useEffect(() => {
        fetchTodos(page, limit);
    }, [page]);

    if (loading) {
        return <h1>Loading...</h1>;
    }
    if (error) {
        return <h1>{error}</h1>;
    }

    return (
        <div>
            {todos.map(t => <div key={t.id}>{t.id} - {t.title}</div>)}
            <div style={{display: 'flex'}}>
                {pages.map((p, i) =>
                    <div
                        key={i}
                        onClick={() => setTodoPage(p)}
                        style={{border: p === page ? '2px solid green' : '1px solid gray', padding: '10px'}}
                    >
                        {p}
                    </div>
                )}
            </div>
        </div>
    );
};

export default TodoList;
