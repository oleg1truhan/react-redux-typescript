import React, {FC, useEffect} from 'react';

import {useActions} from '../hooks/useActions';
import {getUser} from '../selectors';
import {useSelector} from 'react-redux';

const UserList: FC = () => {

    const {users, error, loading} = useSelector(getUser);
    const {fetchUsers} = useActions();

    useEffect(() => {
        fetchUsers();
    }, []);

    if (loading) {
        return <h1>Loading...</h1>;
    }
    if (error) {
        return <h1>{error}</h1>;
    }

    return <div>
        {users.map(u => <div key={u.id}>{u.name}</div>)}
    </div>;
};

export default UserList;
