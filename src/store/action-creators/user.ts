import {Dispatch} from 'redux';
import {UserActions, UserActionsTypes} from '../../type/user';
import axios from 'axios';

export const fetchUsers = () => async (dispatch: Dispatch<UserActions>) => {
    try {
        dispatch({type: UserActionsTypes.FETCH_USERS});
        const response = await axios.get('https://jsonplaceholder.typicode.com/users');
        dispatch({type: UserActionsTypes.FETCH_USERS_SUCCESS, payload: response.data});
    } catch (e) {
        dispatch({type: UserActionsTypes.FETCH_USERS_ERROR, payload: 'Some error occured'});
    }
};
