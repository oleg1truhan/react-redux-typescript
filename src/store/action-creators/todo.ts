import {Dispatch} from 'redux';
import axios from 'axios';
import {TodoActions, TodoActionsTypes} from '../../type/todo';

export const fetchTodos = (page = 1, limit = 10) => async (dispatch: Dispatch<TodoActions>) => {
    try {
        dispatch({type: TodoActionsTypes.FETCH_TODO});
        const response = await axios.get('https://jsonplaceholder.typicode.com/todos', {
            params: {_page: page, _limit: limit}
        });
        dispatch({type: TodoActionsTypes.FETCH_TODO_SUCCESS, payload: response.data});
    } catch (e) {
        dispatch({type: TodoActionsTypes.FETCH_TODO_ERROR, payload: 'Some error occured'});
    }
};

export const setTodoPage = (page: number): TodoActions => ({type: TodoActionsTypes.SET_TODO_PAGE, payload: page});
