import {useDispatch} from 'react-redux';
import {bindActionCreators} from 'redux';
import {allActionsCreator} from '../store/action-creators';

export const useActions = () => {
    const dispatch = useDispatch();
    return bindActionCreators(allActionsCreator, dispatch);
};
